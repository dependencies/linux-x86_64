#!/bin/sh

source tools-cmake.sh

name=plustache

prepare_build ${name}

cmake -DCMAKE_INSTALL_PREFIX=/usr/local/ -DBUILD_SHARED_LIBS=ON ../../../sources/${name}/dependency-${name}/
make install/strip
install_and_pack ${name} dynamic

cmake -DCMAKE_INSTALL_PREFIX=/usr/local/ -DBUILD_SHARED_LIBS=OFF ../../../sources/${name}/dependency-${name}/
make install/strip
install_and_pack ${name} static
