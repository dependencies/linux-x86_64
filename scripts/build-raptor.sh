#!/bin/sh

#!/bin/sh

source tools-cmake.sh

source tools-cmake.sh
name=soprano

prepare_build ${name}

cmake -DCMAKE_INSTALL_PREFIX=/usr/local/ \
-DBUILD_SHARED_LIBS=ON \
../../../sources/${name}/dependency-${name}/

make install/strip
install_and_pack ${name} dynamic
