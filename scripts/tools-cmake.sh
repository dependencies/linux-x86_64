
prepare_build() {
  builddir=temporary-build-$1
  mkdir -p $builddir
  cd $builddir
  rm -vf CMakeCache.txt
}

install_and_pack() {
  # Dirty hack : scans cmake output to know installed files
  files=`make install/strip | sed "s|-- Up-to-date: ||g"  | sed "s|-- Installing: ||g" | grep -v "\-\- Set runtime path of" | grep -v "\-\- Removed runtime" | grep /usr/local`
  
  # Archive files
  tar czf $1-$2.tar.gz $files
  tar xzf $1-$2.tar.gz
  rm -rf ../../$1/$2
  mkdir -p ../../$1/$2
  mv -v usr/local/* ../../$1/$2/
}


