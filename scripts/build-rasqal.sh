#!/bin/sh



builddir=temporary-build-rasqal
mkdir -p $builddir
cd $builddir
  
../../../sources/rasqal/dependency-rasqal/configure
make
make install

tar czf rasqal-dynamic.tar.gz /usr/local/bin/roqet \
			      /usr/local/lib/librasqal.so.3 /usr/local/lib/librasqal.so.3.0.0 \
			      /usr/local/lib/pkgconfig/rasqal.pc \
			      /usr/local/include/rasqal \
			      /usr/local/share/man/man1/rasqal-config.1 /usr/local/share/man/man1/roqet.1 /usr/local/share/man/man3/librasqal.3
tar xzf rasqal-dynamic.tar.gz
rm -rf ../../rasqal/dynamic
mkdir -p ../../rasqal/dynamic
mv -v usr/local/* ../../rasqal/dynamic

