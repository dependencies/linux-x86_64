#!/bin/sh

name=boost
version=1.42.0
version2=1_42_0

builddir=temporary-build-boost
# rm -rf $builddir/*
mkdir -p $builddir
cd $builddir

cd ../../../sources/boost/sources/
# wget http://sourceforge.net/projects/boost/files/boost/${version}/boost_${version2}.tar.bz2
cd -
tar xjvf ../../../sources/boost/sources/boost_${version2}.tar.bz2
mv boost_${version2}/* . && rm -rf boost_${version2}
./bootstrap.sh --prefix=/usr/local/ --with-libraries=system,filesystem,regex
./bjam

rm -f ../../${name}/dynamic/lib/*.so*
mkdir -p ../../${name}/dynamic/lib
mv -v stage/lib/*.so* ../../${name}/dynamic/lib/

mkdir -p ../../${name}/static/lib
rm -f ../../${name}/static/lib/*.a*
mv -v stage/lib/*.a ../../${name}/static/lib/
